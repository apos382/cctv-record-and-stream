# CCTV Record and Stream

A bash script that records and streams in rtsp protocol.
### Notes
- This script does no transcoding it uses the `-vcodec copy -an ` on ffmpeg
- You can of course change any ffmpeg settings according to your need( you might want to add transcoding or sound)

## Installing
1. Download [rtsp-simple-server](https://github.com/aler9/rtsp-simple-server/releases "rtsp-simple-server") for your distribution
2. Extract the files in the same directory as the `cctv.sh`

## Configuring
#### In the `cctv.sh` file edit the following according to your needs:
```bash
camera_url[1]="rtsp://x.x.x.x:554/user=admin&password=admin&channel=2&stream=0./"
camera_url[2]="rtsp://admin:admin@x.x.x.x:554/11"
camera_url[3]="rtsp://admin:admin@x.x.x.x:554/ch01/0"
#...
#camera_url[n-1]="rtsp://...
#camera_url[n]="rtsp://...

rtsp_port="30220"
record_path="/mnt/disks/CCTV/" #The path where the records will be placed
```
Add as many cameras as you want (i have tested this with 3 cameras without any issues)
#### In the `rtsp-simple-server.yml` file edit the following according to your needs:
```
rtspAddress: :8554
```
**Please note thate the `rtsp_port` from cctv.sh and `rtspAddress` parameters should match. **
This will be fixed in a later version of the script.
