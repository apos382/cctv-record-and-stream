#This script is free software: you can redistribute it and/or modify it under the terms of the GNU
#General Public License as published by the Free Software Foundation, either version 3 of the License, or 
#(at your option) any later version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
#the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public
#License for more details.
#You should have received a copy of the GNU General Public License along with this program. If not, see
#<https://www.gnu.org/licenses/>. 



#!/bin/bash
declare -a camera_url pid_read pid_write
#############################
#Uses RTSP Server from here https://github.com/aler9/rtsp-simple-server
#Programs used: rtsp-simple-server, ffmpeg, pgrep, date
#Unpack rtsp-simple-server in the same directory as the script
#
#To be done
#check if processes are present, pid_read[$i]="$!"
#check if dir is mounted
#############################
camera_url[1]="rtsp://192.1.1.9:554/user=admin&password=admin&channel=2&stream=0./"
camera_url[2]="rtsp://admin:admin@192.1.1.10:554/11"
camera_url[3]="rtsp://admin:00000000@192.1.1.12:554/ch01/0"
#...
#camera_url[n-1]="rtsp://...
#camera_url[n]="rtsp://...
rtsp_port="30220"
record_path="/mnt/disks/CCTV/"

#############################

cd  "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" || exit
n="${#camera_url[@]}"
./rtsp-simple-server rtsp-simple-server.yml &
sleep 10s

while [ true ]; do
		echo $pid_write[$i]
	for ((i=1;i<=n;i++)); do
		#write to rtsp server
		if [[ ! -d "/proc/${pid_write[$i]}" || -z ${pid_write[$i]} ]]; then
			ffmpeg -i ${camera_url[$i]} -loglevel error -vcodec copy -an -f rtsp rtsp://127.0.0.1:${rtsp_port}/camera$i &
			pid_write[$i]="$!"
			sleep 5s
		fi
		echo $pid_read[$i]
		#read from rtsp server
		if [[ ! -d "/proc/${pid_read[$i]}" || -z ${pid_read[$i]} ]]; then
			mkdir -p "${record_path%%/}/$(date +%m-%d-%Y)/Camera$i"
			ffmpeg -t 1800 -y -i rtsp://127.0.0.1:${rtsp_port}/camera$i -loglevel error -an -vcodec copy "${record_path%%/}/$(date +%m-%d-%Y)/Camera$i/$(date +%H-%M).mp4" &
			pid_read[$i]="$!"
			#sleep 5s
		fi
	done
	sleep 10s
done
